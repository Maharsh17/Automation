#!/bin/sh

echo "Here Are The Software We Are Going To Install"

echo "Chromium"
echo "Brave"
echo "Visual Code"
echo "Atom"
echo "Android Studio"
echo "Spotify"
echo "Zoom"
echo "Teams"
echo "JDK"
echo "Python-3"
echo "GCC Compiler"

cd
cd Downloads

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Chomium"
sudo apt install chromium-browser -y
echo "Chrome Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Brave"
sudo apt install apt-transport-https curl gnupg
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser -y
echo "Brave Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Visual Code"
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt install code -y
echo "Visual Code Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Atom"
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"
sudo apt install atom -y
echo "Atom Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Spotify"
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 4773BD5E130D1D45
sudo apt update -y
sudo apt install spotify-client -y
echo "Spotify Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Zoom"
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo apt install ./zoom_amd64.deb
echo "Zoom Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Teams"
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main" | sudo tee /etc/apt/sources.list.d/teams.list
sudo apt update
sudo apt install teams -y
echo "Teams Installation Complete"

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing JDK"
sudo apt-get install -y default-jdk
echo "JDK Installation Complete"

echo "Checking Java"
java --version
echo "Checking Javac"
javac --version

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing Python-3"
sudo apt install python3 -y
echo "Python-3 Installation Complete"

echo "Checking Python-3"
python3 --version

echo "Updating And Upgrading"
sudo apt update
sudo apt upgrade
echo "Updating And Upgrading Done"

echo "Installing GCC Compiler"
sudo apt install g++ -y
sudo apt install build-essential -y
echo "GCC Compiler Installation Complete"
